import { motion, useInView, useScroll } from "framer-motion";
import scrollTo from "gatsby-plugin-smoothscroll";
import { Helmet } from "react-helmet";
import { useEffect, useRef, useState } from "react";
import * as React from "react";
import { BarLoader } from "react-spinners";
import styled, { keyframes } from "styled-components";
import Contact from "../components/Contact";
import Home from "../components/Home";
import Expertise from "../components/Expertise";
import Work from "../components/Work";
import "../global.css";
import Sidebar from "../components/Sidebar";

const Layout = styled.main`
  width: 100%;
  height: 100vh;
`;

const shine = keyframes`
  0% { background-position: 0 0; }
  100% { background-position: 0 50px; }
`;

const end = keyframes`
  0%, 100% { box-shadow: 0 0 10px 0 orange; }
  50% { box-shadow: 0 0 15px 5px orange; }
`;

const ProgressBar = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  height: 10px;
  background: linear-gradient(#8fd0e8, #54b3d6, #8fd0e8);
  transform-origin: 0;
  opacity: 0.8;
  transform: scaleX(0) translateZ(0px);
  z-index: 99999;

  transition: transform 0.3s linear;

  box-shadow: 0 0 10px 0 #54b3d6;
  animation: ${shine} 8s ease-in infinite, ${end} 1s ease-out 1 7s;
`;

const Content = styled.div`
  width: 100%;
`;

const Loader = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 20px;
  align-items: center;
  background: #171717;
  color: #fff;
  z-index: 99999999;

  span {
    font-size: 1.3em;
  }
`;

const links = ["About", "Expertise", "Work", "Contact"];

const Index: React.FC = () => {
  const ref = useRef(null);
  const initial = useRef(true);
  const [loading, setLoading] = useState(true);
  const isInView = useInView(ref, { once: true });
  const [page, setPage] = useState<number>(0);
  const { scrollYProgress } = useScroll();

  const gotoPage = e => {
    e.preventDefault();
    const newPage = e.target.dataset.index || 0;
    const id = `#${links[newPage].toLowerCase()}`;
    setPage(parseInt(newPage));
    scrollTo(id);
  };

  useEffect(() => {
    if (initial.current) {
      setTimeout(() => {
        setLoading(false);
        initial.current = false;
      }, 1000);
    }
  }, []);

  return (
    <Layout>
      <Helmet
        htmlAttributes={{
          lang: "en",
        }}
      >
        <meta charSet="utf-8" />
        <title>Robin Kuiper - Backend Developer</title>
        <meta
          name="description"
          content="Versatile backend developer skilled in multiple languages, tools, and frameworks. 
          Create captivating web experiences. Let's collaborate and bring your ideas to life. Contact now!"
        />
        <link rel="canonical" href="https://www.rkuiper.nl" />
        {/*<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120-precomposed.png" />*/}
        {/*<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152-precomposed.png" />*/}
      </Helmet>

      {loading && (
        <Loader>
          <BarLoader size="100px" color="#fff" />
          <span>Initializing</span>
        </Loader>
      )}

      <ProgressBar style={{ scaleX: scrollYProgress }} />
      <Sidebar page={page} doScroll={gotoPage} links={links} isInView={isInView} />

      <Content ref={ref}>
        <Home doScroll={gotoPage} />
        <Expertise />
        <Work />
        <Contact />
      </Content>
    </Layout>
  );
};

export default Index;
