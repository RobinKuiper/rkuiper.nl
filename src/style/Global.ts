import styled from "styled-components";

const Page = styled.div`
  width: 100%;
  min-height: 100vh;
`;

const Center = styled.div`
  width: 100%;
  min-height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  @media (min-width: 400px) and (max-width: 768px) {
    padding: 50px 0;
    justify-content: flex-end;
  }
`;

const Title = styled.h2`
  text-transform: uppercase;

  span {
    color: #54b3d6;
  }
`;

const Link = styled.a`
  font-size: 1.2rem;
  //text-transform: uppercase;
  letter-spacing: 2px;

  box-shadow: inset 0 0 0 0 #54b3d6;
  color: #54b3d6;
  margin: 0 -0.25rem;
  padding: 0.25rem 0.25rem;
  transition: color 0.3s ease-in-out, box-shadow 0.5s ease-in-out;

  &:hover {
    //border-bottom: 2px solid white !important;
    //color: #a8a7a7;
    box-shadow: inset 300px 0 0 0 #54b3d6;
    color: white;

    @media (max-width: 400px) {
      box-shadow: none;
      color: #54b3d6;
    }
  }

  &.active {
    //box-shadow: inset 100px 0 0 0 #54b3d6;
    color: white;
  }
`;

export { Center, Link, Page, Title };
