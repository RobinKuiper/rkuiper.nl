import { motion } from "framer-motion";
import React, { useState } from "react";
import { HashLoader } from "react-spinners";
import styled from "styled-components";
import Nebula from "./animations/nebula";
import { Link, Page, Title } from "../style/Global";
import SocialMenu from "./SocialMenu";

const endPoint = "https://getform.io/f/383b24c8-bcb7-4a1b-965c-f9815e7dda21";

const Contact = styled(Page)`
  background-color: black;
  color: white;
  //box-shadow: 0 -30px 20px -20px rgba(0,0,0,1);
  position: relative;
  height: 100vh;

  @media (max-width: 400px) {
    padding-bottom: 50px;
  }
`;

const Content = styled.div`
  position: absolute;
  left: 25%;
  top: 25%;
  width: 50%;
  height: 50%;
  pointer-events: none;

  a {
    pointer-events: all;
  }

  .loader {
    display: flex;
    margin: 20px 0;
    justify-content: center;

    .bg {
      background: rgba(0, 0, 0, 0.8);
      border-radius: 50%;
    }
  }

  @media (min-width: 400px) and (max-width: 768px) {
    max-width: 100vh;
    width: auto;
    left: 89px;
    padding-right: 10px;
    box-sizing: border-box;
  }

  @media (max-width: 400px) {
    left: 10%;
    width: 80%;
  }
`;

const ContactForm = styled.form`
  margin-top: 30px;
  display: flex;
  flex-direction: column;
  gap: 10px;

  .formgroup {
    display: flex;
    flex-direction: column;
    gap: 3px;

    input,
    textarea {
      padding: 20px 10px;
      background-color: #000;
      color: white;
      border: 1px solid #292929;
      pointer-events: all;
    }

    input {
    }

    textarea {
      height: 150px;
    }
  }

  button {
    pointer-events: all;
    position: relative;
    width: 100%;
    height: 52px;
    background: #fff;
    transform: translate3d(0px, 0%, 0px);
    text-decoration: none;
    font-weight: 600;
    font-size: 18px;
    letter-spacing: 0.05em;
    transition-delay: 0.6s;
    overflow: hidden;

    &:before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: #fff;
      border-radius: 50% 50% 0 0;
      transform: translateY(100%) scaleY(0.5);
      transition: all 0.6s ease;
    }

    &:after {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: #54b3d6;
      border-radius: 0;
      transform: translateY(0) scaleY(1);
      transition: all 0.6s ease;
    }

    div {
      position: relative;
      //top: 50%;
      top: 3px;
      width: 100%;
      height: 26px;
      text-transform: uppercase;
      overflow: hidden;

      span {
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        width: 100%;
        text-align: center;
        transition: transform 0.5s ease;

        &:first-child {
          color: #54b3d6;
          transform: translateY(24px);
        }

        &:last-child {
          color: #fff;
          transform: translateY(0);
        }
      }
    }

    &:hover {
      background: #54b3d6;
      transition: background 0.2s linear;
      transition-delay: 0.6s;
      cursor: pointer;

      &:after {
        border-radius: 0 0 50% 50%;
        transform: translateY(-100%) scaleY(0.5);
        transition-delay: 0s;
      }

      &:before {
        border-radius: 0;
        transform: translateY(0) scaleY(1);
        transition-delay: 0s;
      }

      span:first-child {
        transform: translateY(0);
      }

      span:last-child {
        transform: translateY(-24px);
      }
    }
  }
`;

const Socials = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
  pointer-events: all;
`;

function Layout() {
  const [success, setSuccess] = useState(false);
  const [sending, setSending] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();

    setSending(true);

    const formData = new FormData(e.target);

    const response = await fetch(endPoint, {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      setSuccess(true);
      setSending(false);
    }
  };

  return (
    <Contact id="contact">
      <Nebula />
      <Content>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0,
          }}
          whileInView={{
            opacity: [0, 0.5, 1],
            scale: [0, 1.2, 1],
          }}
          viewport={{ once: true }}
          transition={{
            duration: 1,
          }}
        >
          {success ? (
            <motion.div
              initial={{
                opacity: 0,
              }}
              animate={{
                opacity: 1,
              }}
              transition={{
                duration: 1,
              }}
            >
              <Title>
                Thank <span>you</span>
              </Title>
              <div>
                <p>
                  Thank you for reaching out! Your message has been received loud and clear. I appreciate your time and
                  effort in connecting with me. Rest assured that I will carefully review your message and get back to
                  you as soon as possible. In the meantime, feel free to explore more of my website or check out my{" "}
                  <Link href={"#work"} title={"Work"}>
                    latest projects
                  </Link>
                  . Looking forward to further discussing how we can collaborate and bring your ideas to life. Stay
                  tuned!
                </p>
              </div>
            </motion.div>
          ) : (
            <>
              <Title>
                Get in <span>touch</span>
              </Title>
              {sending && !success ? (
                <motion.div
                  className={"loader"}
                  initial={{
                    opacity: 0,
                  }}
                  animate={{
                    opacity: 1,
                  }}
                  transition={{
                    duration: 1,
                  }}
                >
                  <span className={"bg"}>
                    <HashLoader color={"#54b3d6"} size={"200"} />
                  </span>
                </motion.div>
              ) : (
                <>
                  <p>Shoot me a message with the form below.</p>
                  <ContactForm method={"POST"} action={endPoint} onSubmit={handleSubmit}>
                    <div className={"formgroup"}>
                      <input type="text" name="name" placeholder="Your name" />
                    </div>

                    <div className={"formgroup"}>
                      <input type="email" name="email" placeholder="Your email" />
                    </div>

                    <div className={"formgroup"}>
                      <textarea name="message" placeholder="Your message" />
                    </div>

                    <button type="submit">
                      <div>
                        <span>Send Message</span>
                        <span>Send Message</span>
                      </div>
                    </button>
                  </ContactForm>
                </>
              )}
            </>
          )}
          <Socials>
            <SocialMenu flexDirection={"row"} />
          </Socials>
        </motion.div>
      </Content>
    </Contact>
  );
}

export default Layout;
