import { useInView } from "framer-motion";
import React, { useRef } from "react";
import styled, { keyframes } from "styled-components";
import { Center, Link, Page } from "../style/Global";

const Home = styled(Page)`
  position: relative;
  display: inline-block;

  box-shadow: 0 30px 20px -20px rgba(0, 0, 0, 1);

  background: repeating-linear-gradient(45deg, #0e0d0e 25%, #0e0d0e 50%, #171819 50%, #171819 75%);
  background-size: 5px 7px;

  @media (max-width: 400px) {
    padding-top: 50px;
    padding-bottom: 50px;
  }
`;

const Content = styled.div`
  max-width: 50%;

  p {
    font-family: Roboto, sans-serif;
    font-weight: 400;
    line-height: 2.3;
    margin-top: 30px;
  }

  @media (min-width: 400px) and (max-width: 768px) {
    max-width: 100vh;
    padding-left: 89px;
    padding-right: 10px;
    box-sizing: border-box;
  }

  @media (max-width: 400px) {
    max-width: 80%;
  }
`;

const glitchAnim = keyframes`
  0% {
    clip: rect(89px, 9999px, 77px, 0);
    transform: skew(1deg);
  }
  5% {
    clip: rect(55px, 9999px, 49px, 0);
    transform: skew(0.28deg);
  }
  10% {
    clip: rect(79px, 9999px, 61px, 0);
    transform: skew(0.68deg);
  }
  15% {
    clip: rect(62px, 9999px, 60px, 0);
    transform: skew(0.25deg);
  }
  20% {
    clip: rect(12px, 9999px, 4px, 0);
    transform: skew(0.23deg);
  }
  25% {
    clip: rect(91px, 9999px, 15px, 0);
    transform: skew(0.26deg);
  }
  30% {
    clip: rect(57px, 9999px, 28px, 0);
    transform: skew(0.52deg);
  }
  35% {
    clip: rect(67px, 9999px, 6px, 0);
    transform: skew(0.89deg);
  }
  40% {
    clip: rect(85px, 9999px, 20px, 0);
    transform: skew(0.52deg);
  }
  45% {
    clip: rect(36px, 9999px, 30px, 0);
    transform: skew(0.61deg);
  }
  50% {
    clip: rect(15px, 9999px, 84px, 0);
    transform: skew(0.77deg);
  }
  55% {
    clip: rect(92px, 9999px, 75px, 0);
    transform: skew(0.75deg);
  }
  60% {
    clip: rect(19px, 9999px, 78px, 0);
    transform: skew(0.82deg);
  }
  65% {
    clip: rect(71px, 9999px, 37px, 0);
    transform: skew(0.34deg);
  }
  70% {
    clip: rect(93px, 9999px, 99px, 0);
    transform: skew(0.66deg);
  }
  75% {
    clip: rect(23px, 9999px, 63px, 0);
    transform: skew(0.37deg);
  }
  80% {
    clip: rect(65px, 9999px, 22px, 0);
    transform: skew(0.38deg);
  }
  85% {
    clip: rect(15px, 9999px, 3px, 0);
    transform: skew(0.86deg);
  }
  90% {
    clip: rect(42px, 9999px, 72px, 0);
    transform: skew(0.95deg);
  }
  95% {
    clip: rect(72px, 9999px, 60px, 0);
    transform: skew(0.78deg);
  }
`;

const glitchAnim2 = keyframes`
  0% {
    clip: rect(59px, 9999px, 35px, 0);
    transform: skew(0.24deg);
  }
  5% {
    clip: rect(1px, 9999px, 81px, 0);
    transform: skew(0.79deg);
  }
  10% {
    clip: rect(58px, 9999px, 55px, 0);
    transform: skew(0.09deg);
  }
  15% {
    clip: rect(77px, 9999px, 54px, 0);
    transform: skew(0.36deg);
  }
  20% {
    clip: rect(8px, 9999px, 99px, 0);
    transform: skew(0.64deg);
  }
  25% {
    clip: rect(41px, 9999px, 85px, 0);
    transform: skew(0.27deg);
  }
  30% {
    clip: rect(90px, 9999px, 1px, 0);
    transform: skew(0.83deg);
  }
  35% {
    clip: rect(83px, 9999px, 56px, 0);
    transform: skew(0.67deg);
  }
  40% {
    clip: rect(18px, 9999px, 73px, 0);
    transform: skew(0.88deg);
  }
  45% {
    clip: rect(51px, 9999px, 36px, 0);
    transform: skew(0.71deg);
  }
  50% {
    clip: rect(18px, 9999px, 48px, 0);
    transform: skew(0.11deg);
  }
  55% {
    clip: rect(15px, 9999px, 7px, 0);
    transform: skew(0.64deg);
  }
  60% {
    clip: rect(75px, 9999px, 71px, 0);
    transform: skew(0.03deg);
  }
  65% {
    clip: rect(71px, 9999px, 12px, 0);
    transform: skew(0.4deg);
  }
  70% {
    clip: rect(24px, 9999px, 70px, 0);
    transform: skew(0.29deg);
  }
  75% {
    clip: rect(41px, 9999px, 55px, 0);
    transform: skew(0.28deg);
  }
  80% {
    clip: rect(55px, 9999px, 21px, 0);
    transform: skew(0.83deg);
  }
  85% {
    clip: rect(8px, 9999px, 51px, 0);
    transform: skew(0.37deg);
  }
  90% {
    clip: rect(16px, 9999px, 24px, 0);
    transform: skew(0.63deg);
  }
  95% {
    clip: rect(33px, 9999px, 95px, 0);
    transform: skew(0.63deg);
  }
`;

const glitchSkew = keyframes`
  0% {
    transform: skew(3deg);
  }
  10% {
    transform: skew(-2deg);
  }
  20% {
    transform: skew(3deg);
  }
  30% {
    transform: skew(2deg);
  }
  40% {
    transform: skew(0deg);
  }
  50% {
    transform: skew(1deg);
  }
  60% {
    transform: skew(0deg);
  }
  70% {
    transform: skew(2deg);
  }
  80% {
    transform: skew(3deg);
  }
  90% {
    transform: skew(1deg);
  }
`;

const Title = styled.div`
  h1 {
    //font: 500 8rem 'Montserrat';

    font-size: 3.275em;
    font-weight: 500;
    position: relative;
    text-transform: uppercase;
    letter-spacing: 9.9px;
    color: #fff;

    animation: ${glitchSkew} 1s infinite linear alternate-reverse;

    &:before {
      content: attr(data-text);
      position: absolute;
      top: 0;
      width: 100%;
      height: 100%;
      left: 2px;
      text-shadow: -2px 0 #635860;
      clip: rect(44px, 450px, 56px, 0);
      animation: ${glitchAnim} 5s infinite linear alternate-reverse;
    }

    &:after {
      content: attr(data-text);
      position: absolute;
      top: 0;
      width: 100%;
      height: 100%;
      left: -2px;
      text-shadow: -2px 0 #54b3d6, 2px 2px #084b64;
      clip: rect(44px, 450px, 56px, 0);
      animation: ${glitchAnim2} 5s infinite linear alternate-reverse;
    }
  }

  h2 {
    color: #54b3d6;
  }
`;

const forward = keyframes`
  0% {
    margin-left: 1rem;
  }
  50% {
    margin-left: 1.5rem;
  }
  100% {
    margin-left: 1rem;
  }
`;

const CTA = styled.div`
  margin-top: 30px;

  svg {
    fill: #fff;
    margin-left: 1rem;
    box-sizing: border-box;
    vertical-align: middle;

    animation-name: ${forward};
    animation-duration: 0.8s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
  }
`;

function Layout({ doScroll }) {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });

  return (
    <Home id="about" ref={ref}>
      <Center>
        <Content>
          <Title>
            <h1
              data-text="Hi! I'm Robin Kuiper"
              style={{
                transform: isInView ? "none" : "translateX(-200px)",
                opacity: isInView ? 1 : 0,
                transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 1.2s",
              }}
            >
              Hi! I'm Robin Kuiper
            </h1>
            <h2
              style={{
                transform: isInView ? "none" : "translateX(200px)",
                opacity: isInView ? 1 : 0,
                transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 1.4s",
              }}
            >
              Developer
            </h2>
          </Title>

          <p
            style={{
              transform: isInView ? "none" : "translateX(-200px)",
              opacity: isInView ? 1 : 0,
              transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 1.6s",
            }}
          >
            Unlocking the power of code and creativity, I am a seasoned backend developer with an innate flair for
            frontend magic. With a{" "}
            <Link href="#expertise" data-index={1} onClick={doScroll}>
              repertoire
            </Link>{" "}
            of languages, tools, and frameworks under my belt, I bring versatility and innovation to every project. From
            seamless user interfaces to rock-solid functionality, I thrive on bridging the gap between backend
            intricacies and captivating frontend experiences. Step into my world, where pixels and algorithms dance in
            harmony, and let's create digital wonders together.
            <br />
            Please, don't hesitate to{" "}
            <Link href="#contact" data-index={2} onClick={doScroll}>
              contact
            </Link>{" "}
            me.
          </p>

          <CTA
            style={{
              transform: isInView ? "none" : "translateX(200px)",
              opacity: isInView ? 1 : 0,
              transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 1.8s",
            }}
          >
            <Link href="#expertise" data-index={1} onClick={doScroll}>
              See my expertises
              <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd">
                <path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z"></path>
              </svg>
            </Link>
          </CTA>
        </Content>
      </Center>
    </Home>
  );
}

export default Layout;
