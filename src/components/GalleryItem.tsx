import { useInView } from "framer-motion";
import React, { useRef } from "react";
import styled from "styled-components";

const Item = styled.div`
  position: relative;
  text-align: center;
  background-color: #292929;

  &:hover {
    cursor: pointer;
  }

  img {
    width: 100%;
  }

  .info {
    opacity: 0;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    color: #fff;
    padding: 3px 5px;
    transition: opacity 0.3s linear;
  }

  &:hover .info {
    opacity: 1;
  }
`;

const GalleryItem = ({ data, onClick, index }) => {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });

  const translate = index % 2 === 0 ? -200 : 200;

  return (
    <Item
      ref={ref}
      data-tag={data.tags.join(" ")}
      className={"item"}
      onClick={() => onClick(data)}
      style={{
        transform: isInView ? "none" : `translateX(${translate}px)`,
        opacity: isInView ? 1 : 0,
        transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 0.2s",
      }}
    >
      <div>
        {data.image ?? data.images[0]}

        <div className={"info"}>{data.label}</div>
      </div>
    </Item>
  );
};

export default GalleryItem;
