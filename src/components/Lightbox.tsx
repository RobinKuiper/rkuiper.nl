import React, { useEffect, useState } from "react";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { FaGit, FaLink, FaPlus } from "react-icons/fa";
import styled from "styled-components";
import { Center } from "../style/Global";

const Lightbox = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 999999;
  opacity: 0;
  pointer-events: none;
  transition: opacity 0.3s ease-in-out;

  .overlay {
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: rgba(0, 0, 0, 0.8);
  }

  .navigation {
    position: absolute;
    top: calc(50% - 50px);
    color: #fff;
    cursor: pointer;
    z-index: 9999;

    &:hover {
      color: #b0afaf;
    }

    &.left-button {
      left: 50px;
    }

    &.right-button {
      right: 50px;
    }
  }

  #close {
    position: absolute;
    top: 15px;
    right: 15px;
    color: white;
    transform: rotate(45deg);
    cursor: pointer;
    transition: all 0.3s ease-in-out;
    transform-origin: center;

    &:hover {
      color: #afabab;
      transform: rotate(180deg);
      transform-origin: center;
    }
  }

  &.open {
    opacity: 1;
    pointer-events: all;
  }

  #image {
    position: relative;
    max-width: 80%;
    margin: auto;

    img {
      margin: auto;
      width: 100%;
    }

    #info {
      display: flex;
      justify-content: space-between;
      align-items: center;
      position: absolute;
      bottom: 0;
      width: 100%;
      background-color: rgba(0, 0, 0, 0.4);
      color: #fff;
      padding: 10px 20px;
      box-sizing: border-box;
      opacity: 1;
      transition: opacity 0.3s ease-in-out;

      a {
        &:hover {
          color: #d3d3d3;
        }
      }

      .left {
        display: flex;
        flex-direction: column;
        gap: 10px;
      }

      .right {
        display: flex;
        gap: 25px;
      }

      p {
        margin: 0;
      }
    }

    &:hover #info {
      opacity: 1;
    }
  }
`;

function Layout({ open, setOpen, image = null }) {
  const [activeImageIndex, setActiveImageIndex] = useState(0);

  useEffect(() => {
    const scroll = e => {
      if (open) e.preventDefault();
    };

    const keyup = e => {
      if (open) {
        if (e.code === "Escape") {
          setOpen(false);
        }

        if (e.code === "Space" || e.code.includes("Arrow")) {
          e.preventDefault();
        }
      }
    };

    window.addEventListener("wheel", scroll, { passive: false });
    window.addEventListener("keydown", keyup, { passive: false });

    return () => {
      window.removeEventListener("wheel", scroll);
      window.removeEventListener("keydown", keyup);
    };
  }, [open]);

  const goLeft = () => {
    setActiveImageIndex(state => (state - 1 < 0 ? image.images.length - 1 : state - 1));
  };

  const goRight = () => {
    setActiveImageIndex(state => (state + 1 > image.images.length - 1 ? 0 : state + 1));
  };

  if (!image) return "";

  return (
    <Lightbox className={open ? "open" : ""}>
      <div className={"overlay"} onClick={() => setOpen(false)} />

      {image.images && image.images.length && (
        <div className="navigation left-button" onClick={goLeft}>
          <BsChevronLeft size={100} />
        </div>
      )}

      {image.images && image.images.length && (
        <div className="navigation right-button" onClick={goRight}>
          <BsChevronRight size={100} />
        </div>
      )}

      <div id={"close"} onClick={() => setOpen(false)}>
        <FaPlus size={"25px"} />
      </div>

      <Center>
        <div id={"image"}>
          <div className="images">{image.image ?? image.images[activeImageIndex]}</div>

          <div id={"info"}>
            <div className="left">
              {image.url ? (
                <a href={image.url} title={image.label} target={"_blank"}>
                  <h2>{image.label}</h2>
                </a>
              ) : (
                <h2>{image.label}</h2>
              )}
              <p>{image.description}</p>
            </div>

            <div className="right">
              {image.git && (
                <a href={image.git} title={`${image.label} Source`} target={"_blank"}>
                  <FaGit size={30} />
                </a>
              )}
              {image.url && (
                <a href={image.url} title={image.label} target={"_blank"}>
                  <FaLink size={30} />
                </a>
              )}
            </div>
          </div>
        </div>
      </Center>
    </Lightbox>
  );
}

export default Layout;
