import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Img from "../images/logo.png";
import { Link } from "../style/Global";
import SocialMenu from "./SocialMenu";

const SidebarStyle = styled.nav`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  width: 96px;
  box-sizing: border-box;
  position: fixed;
  top: 48px;
  left: 48px;
  bottom: 48px;
  z-index: 32;
  isolation: isolate;
  padding: 10px 20px;
  background-color: #211e1e; // #050816
  //background: rgba(0,0,0,1);
  //background: repeating-linear-gradient(45deg, #332f2f 25%, #332f2f 50%, #211e1e 50%, #211e1e 75%);
  //background-size: 5px 7px;

  box-shadow: 0 0 20px rgba(0, 0, 0, 1);

  @media (min-width: 400px) and (max-width: 768px) {
    left: 0;
    top: 0;
    height: 100vh;
    width: 70px;
  }

  @media (max-width: 400px) {
    display: block;
    top: 0;
    left: 0;
    width: 100vw;
    bottom: auto;
    opacity: 0;
    transition: opacity 0.3s ease-in-out;
    padding: 0;
    margin: 0;

    &.active {
      opacity: 0.8;
    }

    &.open {
      height: 100%;
      opacity: 1;
    }

    //flex-direction: row;
    //justify-content: space-between;
  }
`;

const SidebarHeading = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;

  @media (max-width: 400px) {
    padding: 10px 20px;
  }
`;

const Logo = styled.img`
  display: flex;
  width: 100%;
  transition: filter 0.2s ease-in-out;

  &:hover {
    filter: invert(50%) sepia(28%) saturate(590%) hue-rotate(180deg) brightness(100%) contrast(80%);
  }

  @media (max-width: 400px) {
    height: 48px;
  }
`;

const SidebarContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  flex: 1 1 auto;
  max-width: 48px;

  @media (max-width: 400px) {
    max-width: 100%;
    //display: block;
    //flex: none;
  }
`;

const SidebarMenu = styled.div`
  transform: rotate(-90deg) translate3d(-50%, 0, 0);
  display: flex;
  flex-direction: row-reverse;
  position: relative;
  top: calc(16px * -1);
  gap: 30px;
  margin-top: 50px;

  @media (max-width: 400px) {
    transform: rotate(0deg) translate3d(0, 0, 0);
    flex-direction: column;
    text-align: center;

    display: none;
    transition: all 0.3s ease-in-out;

    &.open {
      display: flex;
    }
  }
`;

const Socials = styled.div`
  @media (max-width: 400px) {
    display: none;
    position: absolute;
    bottom: 20px;

    &.open {
      display: block;
    }
  }
`;

const Hamburger = styled.button`
  display: none;

  cursor: pointer;
  width: 48px;
  height: 48px;
  transition: all 0.25s;
  margin-right: 25px;
  position: relative;
  background: transparent;
  border: none;

  &:hover [class*="-bun"] {
    background: #f1f1f1;
  }

  .hamburger__top-bun,
  .hamburger__bottom-bun {
    content: "";
    position: absolute;
    left: 25%;
    width: 24px;
    height: 2px;
    background: #fff;
    transform: rotate(0);
    transition: all 0.5s;
  }

  .hamburger__top-bun {
    transform: translateY(-5px);
  }

  .hamburger__bottom-bun {
    transform: translateY(3px);
  }

  &.open {
    transform: rotate(90deg) translateY(-1px);
  }

  &.open .hamburger__top-bun {
    transform: rotate(45deg) translateY(0px);
  }

  &.open .hamburger__bottom-bun {
    transform: rotate(-45deg) translateY(0px);
  }

  @media (max-width: 400px) {
    display: block;
  }
`;

function Sidebar({ page, doScroll, links, isInView }) {
  const [isOpen, setOpen] = useState(false);
  const [atTop, setAtTop] = useState(true);
  const [isMobile, setIsMobile] = useState(false);
  const [className, setClassName] = useState("");

  const handleMenuItemClick = e => {
    setOpen(false);
    doScroll(e);
  };

  const toggleOpen = () => {
    setOpen(!isOpen);
  };

  useEffect(() => {
    const scroll = () => {
      setAtTop(window.scrollY < 50);
    };

    window.addEventListener("scroll", scroll, { passive: false });

    return () => {
      window.removeEventListener("scroll", scroll);
    };
  }, [isOpen]);

  useEffect(() => {
    let className = !atTop ? "active" : "";
    className += isOpen ? " open" : "";
    setClassName(className);
  }, [atTop, isOpen]);

  useEffect(() => {
    if (typeof window === "undefined") {
      setIsMobile(false);
    }

    setIsMobile(window.innerWidth < 400);
  }, []);

  return (
    <SidebarStyle
      style={
        !isMobile
          ? {
              transform: !isMobile && isInView ? "none" : "translateX(-200px) rotate(180deg)",
              opacity: !isMobile && isInView ? 1 : 0,
              transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 2s",
            }
          : {}
      }
      className={className}
    >
      <SidebarHeading>
        <a href="#about" data-index={0} onClick={doScroll}>
          <Logo src={Img} alt={"Robin Kuiper Logo"} />
          {/*<Logo alt="Robin Kuiper" src={Img} />*/}
        </a>

        <Hamburger id="hamburger" className={isOpen ? "open hamburger" : "hamburger"} onClick={toggleOpen}>
          <span className="hamburger__top-bun" />
          <span className="hamburger__bottom-bun" />
        </Hamburger>
      </SidebarHeading>

      <SidebarContent onScroll={doScroll}>
        <SidebarMenu className={isOpen ? "open" : ""}>
          {links.map((link, i) => (
            <Link
              key={link.toLowerCase()}
              href={`#${link.toLowerCase()}`}
              data-index={i}
              onClick={handleMenuItemClick}
              // style={{ borderBottom: i === page ? "2px solid white" : "none" }}
              className={i === page ? "active" : ""}
            >
              {link}
            </Link>
          ))}
        </SidebarMenu>

        <Socials className={isOpen ? "open" : ""}>
          <SocialMenu />
        </Socials>
      </SidebarContent>
    </SidebarStyle>
  );
}

export default Sidebar;
