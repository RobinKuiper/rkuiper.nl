import { StaticImage } from "gatsby-plugin-image";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Link, Title } from "../style/Global";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import GalleryItem from "./GalleryItem";
import gullahGif from "../images/work/gullah.webp";
import ckGif from "../images/work/cknol.webp";
import rkuiperGif from "../images/work/rkuiper2.webp";
import Lightbox from "./Lightbox";

const Work = styled.div`
  color: black;

  position: relative;
  box-shadow: 0 50px 40px -40px rgba(0, 0, 0, 1);
  border-bottom: 5px solid black;
  padding-top: 1px;
  padding-bottom: 1px;
  z-index: 10;
  box-sizing: border-box;

  &:before {
    content: "";
    position: absolute;
    box-shadow: inset 0 -30px 20px -20px rgba(0, 0, 0, 1);
    z-index: 999999999;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    display: block;
    pointer-events: none;
  }
`;

const Content = styled.div`
  width: 70%;
  margin: 50px auto;

  @media (min-width: 400px) and (max-width: 768px) {
    max-width: 100vh;
    width: auto;
    margin: 25px auto;
    padding-left: 89px;
    padding-right: 10px;
    box-sizing: border-box;
  }

  @media (max-width: 400px) {
    margin: auto;
    width: calc(100vh - 400px);
  }

  //padding: 30px;
  //background-color: rgba(255, 255, 255, .8);
  //box-shadow: 0 0 10px 0 rgba(0, 0, 0, .8)
`;

const Gallery = styled.div`
  width: 100%;
  overflow: hidden;

  @media (max-width: 768px) {
    margin-top: 20px;
  }

  @media (max-width: 400px) {
    margin-bottom: 50px;
  }

  nav {
    display: none;
    justify-content: center;
    align-items: center;

    font-size: 1.25rem;

    ul {
      position: relative;
      display: flex;
      flex-direction: row;
      gap: 30px;
      list-style: none;
      border-radius: 5px;
      padding: 1px 10px;

      li {
        z-index: 10;

        a {
          padding: 5px;
          color: #00688c;
        }

        .active {
          box-shadow: inset 300px 0 0 0 #00688c;
          color: white;

          @media (max-width: 400px) {
            box-shadow: none;
            background: #00688c;
          }
        }

        a:hover {
          //border-bottom: 1px solid black;
          //background: #00688c;
        }
      }
    }

    @media (min-width: 768px) {
      display: flex;
    }
  }

  .item {
    max-height: 400px;
    overflow: hidden;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.6);

    img {
      filter: grayscale(0.8);
      transition: filter 0.3s ease-in-out, transform 0.3s ease-in-out;

      &:hover {
        filter: grayscale(0);
        transform: scale(1.5);
      }
    }
  }
`;

interface ImageData {
  label: string;
  image?: React.JSX.Element;
  images?: React.JSX.Element[];
  tags: string[];
  url?: string;
  git?: string;
  description?: string;
}

const images: ImageData[] = [
  {
    label: "Remnant2Tools",
    images: [
      <StaticImage key="frontpage" src={"../images/work/remnant_frontpage.png"} alt={"Remnant 2 Tools"} />,
      <StaticImage key="database" src={"../images/work/remnant_database.png"} alt={"Remnant 2 Tools"} />,
      <StaticImage key="builder" src={"../images/work/remnant_builder.png"} alt={"Remnant 2 Tools"} />,
    ],
    tags: ["websites"],
    url: "https://www.remnant-tools.com",
    git: "https://gitlab.com/RobinKuiper/remnantcollectables",
    description: "Gemaakt met Gatsby.",
  },
  {
    label: "RobinKuiper.eu",
    image: <StaticImage src={"../images/work/robinkuiper.webp"} alt={"RobinKuiper.eu"} />,
    tags: ["websites"],
  },
  {
    label: "Cherissa Knol",
    image: <img src={ckGif} alt={"Cherissa Knol"} />,
    tags: ["websites"],
    url: "https://cherissa.rkuiper.nl",
  },
  {
    label: "Gullah Grub",
    image: <StaticImage src={"../images/work/gullah.webp"} alt={"Gullah Grub"} />,
    tags: ["websites"],
  },
  {
    label: "Gullah Grub",
    image: <StaticImage src={"../images/work/gullah2.webp"} alt={"Gullah Grub"} />,
    tags: ["websites"],
  },
  {
    label: "Gullah Grub",
    image: <img src={gullahGif} alt={"Gullah Grub"} />,
    tags: ["websites"],
  },
  {
    label: "Rkuiper.nl",
    image: <img src={rkuiperGif} alt={"Rkuiper.nl"} />,
    tags: ["websites", "opensource"],
    url: "https://www.rkuiper.nl",
    git: "https://gitlab.com/RobinKuiper/rkuiper.nl",
  },
  {
    label: "NME HS",
    image: <StaticImage src={"../images/work/nme.webp"} alt={"NME HS"} />,
    tags: ["websites"],
  },
  {
    label: "Packaging",
    image: <StaticImage src={"../images/work/packaging.webp"} alt={"Packaging"} />,
    tags: ["websites"],
  },
  {
    label: "Packaging",
    image: <StaticImage src={"../images/work/packaging3.webp"} alt={"Packaging"} />,
    tags: ["websites"],
  },
  {
    label: "Actie Roemenie",
    image: <StaticImage src={"../images/work/roemenie.webp"} alt={"Actie Roemenie"} />,
    tags: ["websites"],
  },
  {
    label: "Roll20 Api Scripts",
    image: <StaticImage src={"../images/work/roll20.webp"} alt={"Roll20 Api Scripts"} />,
    tags: ["scripts", "opensource"],
    url: "https://github.com/robinkuiper/Roll20ApiScripts",
  },
  {
    label: "n00bsterBot",
    image: <StaticImage src={"../images/work/bot.webp"} alt={"n00bsterBot"} />,
    tags: ["scripts", "opensource", "tools"],
    url: "https://gitlab.com/RobinKuiper/n00bsterbot",
  },
  {
    label: "Tabletop Simulator Scripts",
    image: <StaticImage src={"../images/work/tts.webp"} alt={"Tabletop Simulator Scripts"} />,
    tags: ["scripts", "opensource"],
    url: "https://gitlab.com/RobinKuiper/tabletop-simulator-scripts",
  },
];

const tags = [
  {
    tag: "all",
    label: "All",
    active: true,
  },
  {
    tag: "websites",
    label: "Websites",
    active: false,
  },
  {
    tag: "scripts",
    label: "Scripts",
    active: false,
  },
  {
    tag: "opensource",
    label: "Open Source",
    active: false,
  },
];

function Layout() {
  const [tag, setTag] = useState("all");
  const [imgs, setImgs] = useState<ImageData[]>(images);
  const [activeImg, setActiveImage] = useState<ImageData | null>(null);
  const [lbOpen, setLBOpen] = useState(false);

  useEffect(() => {
    let i = images;
    if (tag !== "all") {
      i = images.filter(img => img.tags.includes(tag));
    }
    setImgs(i);
  }, [tag]);

  const handleTagChange = e => {
    e.preventDefault();
    setTag(e.target.dataset.tag);
    document.querySelectorAll(".active").forEach(el => el.classList.remove("active"));
    e.target.classList.add("active");
  };

  const toggleLightbox = (imageData: ImageData) => {
    setActiveImage(imageData);
    setLBOpen(!lbOpen);
  };

  return (
    <>
      <Work id="work">
        <Content>
          <Title>
            What I <span>Made</span>
          </Title>

          <Gallery>
            <nav>
              <ul>
                {tags.map(tag => (
                  <li key={tag.tag}>
                    <Link href="#" data-tag={tag.tag} className={tag.active ? "active" : ""} onClick={handleTagChange}>
                      {tag.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </nav>

            <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 700: 2 }}>
              <Masonry gutter={"30px"}>
                {imgs.map((image, i) => (
                  <GalleryItem key={i} index={i} data={image} onClick={toggleLightbox} />
                ))}
              </Masonry>
            </ResponsiveMasonry>
          </Gallery>
        </Content>
      </Work>

      <Lightbox open={lbOpen} image={activeImg} setOpen={setLBOpen} />
    </>
  );
}

export default Layout;
