import React, { useRef } from "react";
import styled from "styled-components";
import { Center, Page, Title } from "../style/Global";
import { useInView } from "framer-motion";

const Expertise = styled(Page)`
  color: black;
`;

const Content = styled.div`
  max-width: 50%;

  @media (min-width: 400px) and (max-width: 768px) {
    max-width: 100vh;
    padding-left: 89px;
    padding-right: 10px;
    box-sizing: border-box;
  }

  @media (max-width: 400px) {
    max-width: 90%;
  }

  //padding: 30px;
  //background-color: rgba(255, 255, 255, .8);
  //box-shadow: 0 0 10px 0 rgba(0, 0, 0, .8)
`;

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;

  div:first-child {
    flex: 1 1 30%;
  }

  @media (max-width: 768px) {
    flex-direction: column;

    ul {
      margin: 0;
      padding: 0;
    }
  }
`;

const ExpertiseList = styled.div`
  display: flex;
  flex-direction: row;

  ul {
    list-style: none;

    li {
    }

    li:before {
      content: "✓ ";
      //color: #54b3d6;
    }
  }

  span {
    text-align: right;
    width: 100%;
    display: block;
  }
`;

function Layout() {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });

  return (
    <Expertise id="expertise">
      <Center>
        <Content ref={ref}>
          <Title
            style={{
              transform: isInView ? "none" : "translateX(-200px) rotate(180deg)",
              opacity: isInView ? 1 : 0,
              transition: "all 0.7s cubic-bezier(0.17, 0.55, 0.55, 1) 0.2s",
            }}
          >
            What I <span>Love</span>
          </Title>

          <FlexRow>
            <div
              style={{
                transform: isInView ? "none" : "translateX(-200px)",
                opacity: isInView ? 1 : 0,
                transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 0.2s",
              }}
            >
              <p>
                With years of experience in programming, I have worked extensively with a wide range of tools,
                frameworks, and languages.
              </p>

              <p>
                From core languages like C#, PHP, and JavaScript to frameworks such as React, Laravel, and NextJS, my
                expertise spans diverse domains, enabling me to deliver innovative solutions across various platforms.
              </p>
            </div>

            <div
              style={{
                transform: isInView ? "none" : "translateX(200px)",
                opacity: isInView ? 1 : 0,
                transition: "all 0.5s cubic-bezier(0.17, 0.55, 0.55, 1) 0.2s",
              }}
            >
              <p>Some languages, tools and frameworks I worked with:</p>
              <ExpertiseList>
                <div>
                  <ul>
                    <li>HTML5</li>
                    <li>CSS3</li>
                    <li>PHP</li>
                    <li>Javascript</li>
                    <li>MySQL</li>
                    <li>Laravel</li>
                    <li>CodeIgniter</li>
                    <li>Git</li>
                  </ul>
                </div>

                <div>
                  <ul>
                    <li>Meteor.js</li>
                    <li>React</li>
                    <li>Lua</li>
                    <li>Firebase</li>
                    <li>NextJS</li>
                    <li>Gatsby</li>
                    <li>Wordpress</li>
                    <li>Bootstrap</li>
                  </ul>
                </div>

                <div>
                  <ul>
                    <li>jQuery</li>
                    <li>C#</li>
                    <li>Unity</li>
                    <li>Photoshop</li>
                    <li>Android Studio</li>
                    <li>intellij IDEA</li>
                    <li>VSC</li>
                    <li>Python</li>
                  </ul>

                  <span>...and more.</span>
                </div>
              </ExpertiseList>
            </div>
          </FlexRow>
        </Content>
      </Center>
    </Expertise>
  );
}

export default Layout;
