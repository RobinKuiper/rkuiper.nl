import React from "react";
import { AiFillGithub, AiFillGitlab, AiFillLinkedin } from "react-icons/ai";
import { FaDiscord } from "react-icons/fa";
import styled from "styled-components";

const Layout = styled.div`
  display: flex;
  flex-direction: ${props => props.flexDirection};
  gap: ${props => props.gap};

  @media (max-width: 400px) {
    flex-direction: row;
    gap: 20px;
  }

  a {
    color: #fff;

    transition: all 0.3s ease-in-out;

    &:hover {
      color: #54b3d6;
    }
  }
`;

SocialMenu.propTypes = {};

const socialLinks = [
  {
    platform: "LinkedIn",
    icon: <AiFillLinkedin size={"1.5em"} />,
    href: "https://www.linkedin.com/in/robin-kuiper-4a15a669/",
  },
  {
    platform: "Gitlab",
    icon: <AiFillGitlab size={"1.5em"} />,
    href: "https://gitlab.com/robinkuiper",
  },
  {
    platform: "Github",
    icon: <AiFillGithub size={"1.5em"} />,
    href: "https://github.com/RobinKuiper",
  },
  {
    platform: "Discord",
    icon: <FaDiscord size={"1.5em"} />,
    href: "https://discordapp.com/users/81445002168774656",
  },
];

function SocialMenu({ flexDirection = "column", gap = "5px" }) {
  return (
    <Layout flexDirection={flexDirection} gap={gap}>
      {socialLinks.map(social => (
        <div key={social.platform}>
          <a href={social.href} target={"_blank"}>
            {social.icon}
          </a>
        </div>
      ))}
    </Layout>
  );
}

export default SocialMenu;
